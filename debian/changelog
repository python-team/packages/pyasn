pyasn (1.6.2-1) unstable; urgency=medium

  [ Alexandre Detiste ]
  * trim obsolete comment about Python2

  [ Hans-Christoph Steiner ]
  * New upstream version 1.6.2

 -- Hans-Christoph Steiner <hans@eds.org>  Wed, 05 Mar 2025 18:56:05 +0100

pyasn (1.6.1-5) unstable; urgency=medium

  * Team upload.
  * Also switch autopkgtest also from Nose to Pytest

 -- Alexandre Detiste <tchet@debian.org>  Thu, 12 Sep 2024 23:15:44 +0200

pyasn (1.6.1-4) unstable; urgency=medium

  * Team upload.
  * Repair d/watch

 -- Alexandre Detiste <tchet@debian.org>  Sat, 24 Aug 2024 12:51:32 +0200

pyasn (1.6.1-3.2) unstable; urgency=medium

  * Team upload.
  * Replace python3-nose by python3-pytest (Closes: #1018443)

 -- Alexandre Detiste <tchet@debian.org>  Sun, 04 Aug 2024 02:18:39 +0200

pyasn (1.6.1-3.1) unstable; urgency=medium

  * Team upload.
  * Add fix-raw-string.patch (Closes: #1070219).

 -- Thomas Goirand <zigo@debian.org>  Thu, 02 May 2024 08:47:19 +0200

pyasn (1.6.1-3) unstable; urgency=medium

  * Team upload.
  * Support multiple supported Python 3 versions. (Closes: #999402)
  * Use autopkgtest-pkg-python for the import test.
  * Run autopkgtest on all supported Python 3 versions. Run it out of the
    source tree, against the installed package.
  * Bump watch file format to 4.
  * Bump Standards-Version to 4.6.0, no changes needed.

 -- Stefano Rivera <stefanor@debian.org>  Sat, 20 Nov 2021 16:20:11 -0400

pyasn (1.6.1-2) unstable; urgency=medium

  [ Hans-Christoph Steiner ]
  * fix autopkgtest

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Hans-Christoph Steiner <hans@eds.org>  Sun, 03 Jan 2021 20:52:15 +0100

pyasn (1.6.1-1) unstable; urgency=medium

  * Initial release (Closes: #969424)

 -- Hans-Christoph Steiner <hans@eds.org>  Tue, 25 Aug 2020 17:55:33 +0200
